{
    "id": "46f33ac3-3c54-44b8-bbe1-b4bb83faecad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d41dfd79-fd98-4c6d-a3c0-b7f9f245f3e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46f33ac3-3c54-44b8-bbe1-b4bb83faecad",
            "compositeImage": {
                "id": "b8ab4417-52da-4a5d-ab33-73f515bf234b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d41dfd79-fd98-4c6d-a3c0-b7f9f245f3e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96d09da3-89b3-46d2-8b2b-322e40deea9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d41dfd79-fd98-4c6d-a3c0-b7f9f245f3e3",
                    "LayerId": "5c88c4cc-3b19-484b-8c60-260a0cb0ed8c"
                }
            ]
        },
        {
            "id": "1eae3335-ef6b-4338-b348-11ec04955721",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46f33ac3-3c54-44b8-bbe1-b4bb83faecad",
            "compositeImage": {
                "id": "85c55d85-3297-4d8c-b41a-809698f20f64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eae3335-ef6b-4338-b348-11ec04955721",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f1318d4-25d3-4d0f-a1b6-815cbee6df38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eae3335-ef6b-4338-b348-11ec04955721",
                    "LayerId": "5c88c4cc-3b19-484b-8c60-260a0cb0ed8c"
                }
            ]
        },
        {
            "id": "9f903810-476f-46de-baab-538699a2e2c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46f33ac3-3c54-44b8-bbe1-b4bb83faecad",
            "compositeImage": {
                "id": "611f0e10-8f81-409f-a987-91f976bb6910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f903810-476f-46de-baab-538699a2e2c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c5f09f1-a8ba-477e-94a7-09c38b0e217b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f903810-476f-46de-baab-538699a2e2c3",
                    "LayerId": "5c88c4cc-3b19-484b-8c60-260a0cb0ed8c"
                }
            ]
        },
        {
            "id": "d98f4cbf-3f0b-48e8-a7b1-174fdba2ed74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46f33ac3-3c54-44b8-bbe1-b4bb83faecad",
            "compositeImage": {
                "id": "1963c72c-a6e8-4ae6-998f-b7401a38d6c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d98f4cbf-3f0b-48e8-a7b1-174fdba2ed74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc4f041c-c4b0-4abf-8779-a4e3b7e2a88a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d98f4cbf-3f0b-48e8-a7b1-174fdba2ed74",
                    "LayerId": "5c88c4cc-3b19-484b-8c60-260a0cb0ed8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "5c88c4cc-3b19-484b-8c60-260a0cb0ed8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46f33ac3-3c54-44b8-bbe1-b4bb83faecad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}