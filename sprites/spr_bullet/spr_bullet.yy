{
    "id": "79c18cc0-6531-41ad-880e-78ebb2454c7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c4b2ead-bc87-4829-a626-95a1344b98c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c18cc0-6531-41ad-880e-78ebb2454c7f",
            "compositeImage": {
                "id": "d8094cb4-e183-4fa1-ba49-7ab26a8f7954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c4b2ead-bc87-4829-a626-95a1344b98c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d7726ef-3563-4b8b-987a-000d2ace0968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c4b2ead-bc87-4829-a626-95a1344b98c1",
                    "LayerId": "ed4d52c8-8642-4178-83a3-ae176191ba82"
                }
            ]
        },
        {
            "id": "a1faf144-08a3-4a9b-b752-3aa262d71c7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c18cc0-6531-41ad-880e-78ebb2454c7f",
            "compositeImage": {
                "id": "1b3de4b2-484c-4ad7-a0cc-397734298672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1faf144-08a3-4a9b-b752-3aa262d71c7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78a909af-395f-4e75-971f-3c70d2ddf65a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1faf144-08a3-4a9b-b752-3aa262d71c7d",
                    "LayerId": "ed4d52c8-8642-4178-83a3-ae176191ba82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "ed4d52c8-8642-4178-83a3-ae176191ba82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79c18cc0-6531-41ad-880e-78ebb2454c7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 0,
    "yorig": 0
}