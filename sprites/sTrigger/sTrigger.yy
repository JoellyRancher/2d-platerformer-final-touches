{
    "id": "4ddf9fd2-730c-4a6c-b4f3-260abeed454a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0998ba4c-d65c-486a-a541-05d9518965b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ddf9fd2-730c-4a6c-b4f3-260abeed454a",
            "compositeImage": {
                "id": "00d71bc6-3f0e-445c-86d4-374f8fac99f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0998ba4c-d65c-486a-a541-05d9518965b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c28c882-0849-49a8-85fe-0db057f49294",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0998ba4c-d65c-486a-a541-05d9518965b6",
                    "LayerId": "efbb4e8e-8520-45fb-930a-20a9641f0a89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "efbb4e8e-8520-45fb-930a-20a9641f0a89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ddf9fd2-730c-4a6c-b4f3-260abeed454a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}