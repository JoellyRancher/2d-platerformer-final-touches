{
    "id": "15fd6cb9-479e-4c4f-a2f0-9d57e0603b6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Trees",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18a9f4d2-a114-40de-a165-a97b5e5cde84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15fd6cb9-479e-4c4f-a2f0-9d57e0603b6a",
            "compositeImage": {
                "id": "4d5a7cfc-e1e1-494a-ba61-604a91538a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18a9f4d2-a114-40de-a165-a97b5e5cde84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "427607b8-8e20-47e0-a89d-cb0a496f3eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18a9f4d2-a114-40de-a165-a97b5e5cde84",
                    "LayerId": "4d690cab-50cf-46df-8d46-d4758fe24da4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "4d690cab-50cf-46df-8d46-d4758fe24da4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15fd6cb9-479e-4c4f-a2f0-9d57e0603b6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}