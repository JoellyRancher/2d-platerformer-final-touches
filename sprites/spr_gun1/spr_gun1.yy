{
    "id": "e4c4cc0d-8e4f-4d5c-8d4f-eba23c35b8f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "707c9721-7d3e-4c8f-accf-851a1a4431f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4c4cc0d-8e4f-4d5c-8d4f-eba23c35b8f1",
            "compositeImage": {
                "id": "3269ad01-d671-45e2-b88a-3a2b559c8678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "707c9721-7d3e-4c8f-accf-851a1a4431f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d8e1aa-5760-4797-8daf-4a77e2e86dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "707c9721-7d3e-4c8f-accf-851a1a4431f2",
                    "LayerId": "8a6d6e6f-031f-43d3-8612-2c6f53fc4b77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8a6d6e6f-031f-43d3-8612-2c6f53fc4b77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4c4cc0d-8e4f-4d5c-8d4f-eba23c35b8f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 16
}