{
    "id": "801511f3-1d2c-449b-87c9-ede1beb35748",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eec76b80-f5fc-4ac8-8f29-6f8551e9b7ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801511f3-1d2c-449b-87c9-ede1beb35748",
            "compositeImage": {
                "id": "98371992-7362-45cf-bf6a-3d540b232978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eec76b80-f5fc-4ac8-8f29-6f8551e9b7ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e892afa0-200d-40ad-8233-0445a45394aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eec76b80-f5fc-4ac8-8f29-6f8551e9b7ca",
                    "LayerId": "9d250583-4208-4727-82ae-95d847ec70fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9d250583-4208-4727-82ae-95d847ec70fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "801511f3-1d2c-449b-87c9-ede1beb35748",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}