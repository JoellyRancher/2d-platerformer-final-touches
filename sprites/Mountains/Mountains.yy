{
    "id": "ec8ef968-d855-4975-9bfe-68e8f1594a04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Mountains",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 110,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6649b8e6-d12c-4103-a901-5b43608f8d5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec8ef968-d855-4975-9bfe-68e8f1594a04",
            "compositeImage": {
                "id": "e678e596-96eb-49fe-bb1a-46202df744c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6649b8e6-d12c-4103-a901-5b43608f8d5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e526333-fcc9-4a93-a75a-263b4636b558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6649b8e6-d12c-4103-a901-5b43608f8d5b",
                    "LayerId": "5f460e6c-b07e-42bf-a44a-228a2ae0ef8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "5f460e6c-b07e-42bf-a44a-228a2ae0ef8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec8ef968-d855-4975-9bfe-68e8f1594a04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}