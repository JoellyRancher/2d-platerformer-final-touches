{
    "id": "4d5fed9c-a4aa-49b4-8fc8-1bfa6c269118",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c17d039f-bac2-40fb-8e04-6e495646c61d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5fed9c-a4aa-49b4-8fc8-1bfa6c269118",
            "compositeImage": {
                "id": "71211aa1-20f0-42d0-b415-3b163e62db00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c17d039f-bac2-40fb-8e04-6e495646c61d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30ca037b-c971-4ed8-8b52-18c136074540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c17d039f-bac2-40fb-8e04-6e495646c61d",
                    "LayerId": "781db976-1db3-476b-9e8c-df7f7860e114"
                }
            ]
        },
        {
            "id": "ba6e3648-6620-4efe-8884-974516776aeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5fed9c-a4aa-49b4-8fc8-1bfa6c269118",
            "compositeImage": {
                "id": "f0ea7c9c-037a-4711-97cb-4566bf887d03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba6e3648-6620-4efe-8884-974516776aeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b7586e6-06e2-4686-98e7-795e766801c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba6e3648-6620-4efe-8884-974516776aeb",
                    "LayerId": "781db976-1db3-476b-9e8c-df7f7860e114"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "781db976-1db3-476b-9e8c-df7f7860e114",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d5fed9c-a4aa-49b4-8fc8-1bfa6c269118",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}