{
    "id": "30be4f8a-5aec-46a9-8c61-36b09d354b87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44bb15b2-d392-4292-ac4e-2897f42ff15f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30be4f8a-5aec-46a9-8c61-36b09d354b87",
            "compositeImage": {
                "id": "b5bc101e-d53f-46cb-83f4-77e3d3439d5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44bb15b2-d392-4292-ac4e-2897f42ff15f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "276e82a0-1fc3-43aa-bd07-fd09450ba582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44bb15b2-d392-4292-ac4e-2897f42ff15f",
                    "LayerId": "aaf28cb0-f005-4df6-ba7d-c75a0ae67ae3"
                }
            ]
        },
        {
            "id": "82b0c91d-dbe2-48a7-a7f0-f13c9a8f7c89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30be4f8a-5aec-46a9-8c61-36b09d354b87",
            "compositeImage": {
                "id": "cc756839-c268-4f25-bc93-391f8a80ebfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b0c91d-dbe2-48a7-a7f0-f13c9a8f7c89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a14d6b-dfd7-4860-ae14-9670797a211d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b0c91d-dbe2-48a7-a7f0-f13c9a8f7c89",
                    "LayerId": "aaf28cb0-f005-4df6-ba7d-c75a0ae67ae3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "aaf28cb0-f005-4df6-ba7d-c75a0ae67ae3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30be4f8a-5aec-46a9-8c61-36b09d354b87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}