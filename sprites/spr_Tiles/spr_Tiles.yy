{
    "id": "91007552-36f7-45c6-9669-d4e7036856f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f566e1e3-72a6-4caa-8059-7483eb382e12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91007552-36f7-45c6-9669-d4e7036856f1",
            "compositeImage": {
                "id": "5fe4f8b6-2916-4394-ac0b-82af724145bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f566e1e3-72a6-4caa-8059-7483eb382e12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01fd6c53-b013-4a86-affb-8e6fb3282fe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f566e1e3-72a6-4caa-8059-7483eb382e12",
                    "LayerId": "d90836f4-2ffe-4eca-98a4-702a83d4c6d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "d90836f4-2ffe-4eca-98a4-702a83d4c6d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91007552-36f7-45c6-9669-d4e7036856f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}