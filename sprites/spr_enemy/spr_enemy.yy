{
    "id": "2d2b909e-8567-4b80-b313-90798ba5e9d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8235616e-e9da-4b89-9a9d-aac9c4a9bd39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2b909e-8567-4b80-b313-90798ba5e9d6",
            "compositeImage": {
                "id": "2a794bc8-c4f1-4aec-bb1d-1144a1e1f33c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8235616e-e9da-4b89-9a9d-aac9c4a9bd39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "720f1264-3f42-48c0-944e-1c8ad5fe37f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8235616e-e9da-4b89-9a9d-aac9c4a9bd39",
                    "LayerId": "41f284e0-aabf-45da-b237-ee6e88c4e6dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "41f284e0-aabf-45da-b237-ee6e88c4e6dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d2b909e-8567-4b80-b313-90798ba5e9d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}