{
    "id": "1d5a5f93-b2fc-4d13-a20e-145cb79db8ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 166,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afff1915-6dfa-46a4-a0d5-c7b76ca95e6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5a5f93-b2fc-4d13-a20e-145cb79db8ac",
            "compositeImage": {
                "id": "4b4e4c94-2d21-4870-8506-2363ab47e5b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afff1915-6dfa-46a4-a0d5-c7b76ca95e6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5ae0cab-1b45-4574-9d79-519109c91085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afff1915-6dfa-46a4-a0d5-c7b76ca95e6d",
                    "LayerId": "3e26082a-fdab-410e-bb92-97e46a767f2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 167,
    "layers": [
        {
            "id": "3e26082a-fdab-410e-bb92-97e46a767f2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d5a5f93-b2fc-4d13-a20e-145cb79db8ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}