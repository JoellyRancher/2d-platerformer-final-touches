{
    "id": "ad6fd3cf-a869-4349-8507-6f0767d1cd11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Tile_Ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 248,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2779616-db75-4286-a7c8-9fac1dc24571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad6fd3cf-a869-4349-8507-6f0767d1cd11",
            "compositeImage": {
                "id": "c121545b-b957-4a80-9648-1d251bf261c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2779616-db75-4286-a7c8-9fac1dc24571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b39328c-f5bb-403b-a029-a85cdaef5bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2779616-db75-4286-a7c8-9fac1dc24571",
                    "LayerId": "9d9003ad-922e-4cf2-b49b-cb41d2e94227"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "9d9003ad-922e-4cf2-b49b-cb41d2e94227",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad6fd3cf-a869-4349-8507-6f0767d1cd11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 249,
    "xorig": 0,
    "yorig": 0
}