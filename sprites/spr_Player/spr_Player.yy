{
    "id": "8c8b57d6-31af-47fe-aae0-94a55030243d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80ff8857-1911-49b9-acf2-e160737fe9c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c8b57d6-31af-47fe-aae0-94a55030243d",
            "compositeImage": {
                "id": "a1a013b9-1936-45d7-9e57-0b4c32d5d551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80ff8857-1911-49b9-acf2-e160737fe9c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35335585-fbbe-420a-86fb-bb77c4f1ab93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80ff8857-1911-49b9-acf2-e160737fe9c7",
                    "LayerId": "081ee21e-72b1-4fea-881e-b9eba153a4e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "081ee21e-72b1-4fea-881e-b9eba153a4e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c8b57d6-31af-47fe-aae0-94a55030243d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}