{
    "id": "1f99a7c7-1759-41a5-96ec-f11b7fcc0a2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "071d2415-198c-42c1-be1d-d4898a591680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f99a7c7-1759-41a5-96ec-f11b7fcc0a2e",
            "compositeImage": {
                "id": "87f331c3-baf6-4d0a-beb7-17661b116267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "071d2415-198c-42c1-be1d-d4898a591680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f972576-9fa0-4ada-9ec9-867e26538045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "071d2415-198c-42c1-be1d-d4898a591680",
                    "LayerId": "f8472275-b21c-44bf-a804-2aaaa623eb07"
                }
            ]
        },
        {
            "id": "d0cfdcfb-58b2-4ebd-974d-ee85e5f7c2b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f99a7c7-1759-41a5-96ec-f11b7fcc0a2e",
            "compositeImage": {
                "id": "5c377269-c44b-4421-b896-31bd8231c4c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0cfdcfb-58b2-4ebd-974d-ee85e5f7c2b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a7061f-078c-4e34-90e1-3e5e83df00ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0cfdcfb-58b2-4ebd-974d-ee85e5f7c2b7",
                    "LayerId": "f8472275-b21c-44bf-a804-2aaaa623eb07"
                }
            ]
        },
        {
            "id": "3cdb45c8-bee1-41c2-b693-c438d1c31bf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f99a7c7-1759-41a5-96ec-f11b7fcc0a2e",
            "compositeImage": {
                "id": "8972a074-87d3-4ff5-b0ec-c4fad08bf78b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cdb45c8-bee1-41c2-b693-c438d1c31bf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1480a1f7-2c49-4d81-9293-1d330fb80d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cdb45c8-bee1-41c2-b693-c438d1c31bf8",
                    "LayerId": "f8472275-b21c-44bf-a804-2aaaa623eb07"
                }
            ]
        },
        {
            "id": "7169cd38-4a40-4ce4-9979-c4758e5be928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f99a7c7-1759-41a5-96ec-f11b7fcc0a2e",
            "compositeImage": {
                "id": "ee86a5ec-526f-4c2c-9d2f-dc824dfe1862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7169cd38-4a40-4ce4-9979-c4758e5be928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d61703-2544-4c20-a034-a3278d75031b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7169cd38-4a40-4ce4-9979-c4758e5be928",
                    "LayerId": "f8472275-b21c-44bf-a804-2aaaa623eb07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f8472275-b21c-44bf-a804-2aaaa623eb07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f99a7c7-1759-41a5-96ec-f11b7fcc0a2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}