{
    "id": "f4bb8ee0-5b03-4dcc-9eb0-020a5e9a0ed6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a527a9b0-9ca6-418a-acc0-9be48b74ed70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4bb8ee0-5b03-4dcc-9eb0-020a5e9a0ed6",
            "compositeImage": {
                "id": "044dba17-2826-471e-94e4-f6f4f57f492a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a527a9b0-9ca6-418a-acc0-9be48b74ed70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fb045df-151e-4ad7-a029-e0e84a045bc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a527a9b0-9ca6-418a-acc0-9be48b74ed70",
                    "LayerId": "473c6aaf-c078-4712-bf55-ae5877d6a8b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "473c6aaf-c078-4712-bf55-ae5877d6a8b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4bb8ee0-5b03-4dcc-9eb0-020a5e9a0ed6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 14,
    "yorig": 6
}