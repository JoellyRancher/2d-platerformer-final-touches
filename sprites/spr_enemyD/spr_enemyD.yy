{
    "id": "a932325c-afab-4e5c-8506-afedc0048711",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 3,
    "bbox_right": 47,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da6da8f0-3b1f-4b66-8e89-97feba4aa1a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a932325c-afab-4e5c-8506-afedc0048711",
            "compositeImage": {
                "id": "f5dc2425-89f5-4a22-8908-1301722d9bb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da6da8f0-3b1f-4b66-8e89-97feba4aa1a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4665296b-60f4-432c-93b1-1c2e2b4e3447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da6da8f0-3b1f-4b66-8e89-97feba4aa1a0",
                    "LayerId": "0af2cbcf-5b6b-45cf-9fc0-1b8261180e08"
                }
            ]
        },
        {
            "id": "26876409-e89a-4f9c-9d03-a75547c9176c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a932325c-afab-4e5c-8506-afedc0048711",
            "compositeImage": {
                "id": "498e6d20-5e87-413f-9d1e-f35facfca94e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26876409-e89a-4f9c-9d03-a75547c9176c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041aa128-2378-4927-8c94-77514ac60eac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26876409-e89a-4f9c-9d03-a75547c9176c",
                    "LayerId": "0af2cbcf-5b6b-45cf-9fc0-1b8261180e08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0af2cbcf-5b6b-45cf-9fc0-1b8261180e08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a932325c-afab-4e5c-8506-afedc0048711",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}