{
    "id": "10991b3b-015d-4bce-8eee-e1d043ac1b88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 3,
    "bbox_right": 47,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a38fca49-8737-434b-95b2-3f9860e7302a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10991b3b-015d-4bce-8eee-e1d043ac1b88",
            "compositeImage": {
                "id": "fad63718-8bac-4e13-82f6-95cd89398916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a38fca49-8737-434b-95b2-3f9860e7302a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec571911-e9dd-4b6d-8b52-4650727610fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a38fca49-8737-434b-95b2-3f9860e7302a",
                    "LayerId": "c26620a1-f43d-4743-8965-e7b9d2580c6a"
                }
            ]
        },
        {
            "id": "8de3323a-4e4f-4507-a464-9d6b44a453b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10991b3b-015d-4bce-8eee-e1d043ac1b88",
            "compositeImage": {
                "id": "262c502a-f8cc-4dc3-ae97-251d433fc18b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8de3323a-4e4f-4507-a464-9d6b44a453b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e117a308-0860-4813-aed9-ff7dd254f8f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de3323a-4e4f-4507-a464-9d6b44a453b9",
                    "LayerId": "c26620a1-f43d-4743-8965-e7b9d2580c6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c26620a1-f43d-4743-8965-e7b9d2580c6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10991b3b-015d-4bce-8eee-e1d043ac1b88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}