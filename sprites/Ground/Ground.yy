{
    "id": "9ea1d256-103d-4ec3-941b-74c91a65b836",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6135dbe9-f19a-42e7-af76-3eafd9579d54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ea1d256-103d-4ec3-941b-74c91a65b836",
            "compositeImage": {
                "id": "5d3d07de-dcb5-4740-aa3a-9613003f366c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6135dbe9-f19a-42e7-af76-3eafd9579d54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53256d00-079f-4c51-8b2d-5c9e2d054f28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6135dbe9-f19a-42e7-af76-3eafd9579d54",
                    "LayerId": "dcaaeb60-f750-4676-80e8-0dda337ecdf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "dcaaeb60-f750-4676-80e8-0dda337ecdf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ea1d256-103d-4ec3-941b-74c91a65b836",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}