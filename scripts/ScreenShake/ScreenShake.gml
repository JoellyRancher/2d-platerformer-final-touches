/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 081E108E
/// @DnDArgument : "code" "/// @desc ScreenShake(magnitude, frames)$(13_10)/// @arg Magnitude sets the strength of the shake (radius in pixels)$(13_10)/// @aeg Frames sets the length of the shake in frames (60 = 1 second at 60fps)$(13_10)$(13_10)with (obj_camera)$(13_10){$(13_10)	if (argument0 > shake_remain)$(13_10)	{$(13_10)		shake_magnitude = argument0;$(13_10)		shake_remain = argument0;$(13_10)		shake_length = argument1;$(13_10)	}$(13_10)}"
/// @desc ScreenShake(magnitude, frames)
/// @arg Magnitude sets the strength of the shake (radius in pixels)
/// @aeg Frames sets the length of the shake in frames (60 = 1 second at 60fps)

with (obj_camera)
{
	if (argument0 > shake_remain)
	{
		shake_magnitude = argument0;
		shake_remain = argument0;
		shake_length = argument1;
	}
}