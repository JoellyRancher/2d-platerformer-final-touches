{
    "id": "053007b1-6a13-4389-9dbd-603977088b94",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "19231ea2-f653-4f70-9b6c-ca8b77e3d90b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 47,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 149
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e33c87eb-60ce-4e55-9a65-9bb847ec639f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 47,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 455,
                "y": 100
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "caffa3ea-ecd6-4646-b9c6-ce9cce311fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 232,
                "y": 100
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "76a0541b-987d-4873-a109-ab3eb509c046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 47,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "dca2e217-dac0-41a4-a475-c47828b3efab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 246,
                "y": 100
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "35ab1d95-2b58-4f06-b4ae-670cd334050d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 47,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "04fcbc4c-d30d-4bf1-ac49-63a95668866a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 51
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6a00e76c-07de-44d0-a07e-12ad579de690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 47,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 68,
                "y": 149
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "75238196-74f7-4bf7-a019-1f02d99fab52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 433,
                "y": 100
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d488bccc-97fa-4a9b-b43a-e5e485fa79f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 422,
                "y": 100
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8960a2e4-6cab-4156-9138-ad7b61db1a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 260,
                "y": 100
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bf0d02ca-c92a-4a09-aed9-74f7e6fd0c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 310,
                "y": 51
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "964ab8ff-314c-4d62-aa41-f5275513ec31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 485,
                "y": 100
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1be04ca6-436f-4d64-9236-569c4df0fc92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 47,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 12,
                "y": 149
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1972a923-3249-423f-a065-60c775607ec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 2,
                "y": 149
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f72cbad0-a9e8-49a9-ae19-01f70a63c3dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 47,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 326,
                "y": 100
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "992d1c91-be44-4276-9c68-1bf3d503bfbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 401,
                "y": 51
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f9c4e581-f9ba-4021-9df5-882600f4f41e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 47,
                "offset": 6,
                "shift": 18,
                "w": 7,
                "x": 59,
                "y": 149
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d7ce2a3a-4370-4219-9ae0-b526e1ee47e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 419,
                "y": 51
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "04e1ac8a-670d-47bd-b46f-ff9e43a54a31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 288,
                "y": 100
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1c023250-9a5d-4967-a8a6-fe48fcaa2a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 329,
                "y": 51
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "428cd308-c848-4703-83c1-57c6f4d62d6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 156,
                "y": 100
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "59aa2d01-e49b-48d0-9061-e61b572ff82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 487,
                "y": 2
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "90e08712-b060-4975-a9f2-51c7f2dd0c5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 491,
                "y": 51
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9cd9a920-da15-4f1b-b347-46770c7b6172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 455,
                "y": 51
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "02e2f7ed-dd97-403d-8df5-cfc193e88a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c07b2a2a-9054-497b-978e-5b318a595849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 475,
                "y": 100
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8d64b89d-5a6d-4317-a432-4473f278e2ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 465,
                "y": 100
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f73b7ed8-e450-4d80-a497-b9daf9818048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 172,
                "y": 100
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "784c94a4-2e36-45b2-87ea-52457863b60e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 291,
                "y": 51
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "81cfdec0-c813-4691-b3ed-0ec77aecfcf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 140,
                "y": 100
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b83b0b23-0e41-4b5d-a418-51ef840a8718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 123,
                "y": 100
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "619e882f-38cb-423e-ae5a-17c2a661a521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 47,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "59b56bf1-400b-4608-af0b-2e2056ab8983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 272,
                "y": 51
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "acb27c9b-8f24-471b-8989-6e4a59e89930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 347,
                "y": 51
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c19e8f13-0f15-41ab-847e-e852a8a8bf53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7bb53a1f-1b58-4f60-9828-3b20dc3f5d89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 47,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "603507fe-9d8c-4133-800f-19b785be232f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 188,
                "y": 100
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5499b8e0-6efd-4d5b-891e-5cb1cbeef851",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 16,
                "x": 437,
                "y": 51
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "78332c27-90a5-4329-8b0e-bb227af71375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 47,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 279,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7a196c2f-bd0e-45d5-bcce-68e83894440f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 47,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 2,
                "y": 100
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2f8f14bc-b68a-4507-98c4-96092bb0a1e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 47,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 50,
                "y": 149
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "943e4964-aed7-4067-a400-1d58d8039037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 47,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 350,
                "y": 100
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b343cb43-31a0-4eb0-940d-e6ba74833ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 345,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0d1f5a13-8b53-4604-9260-0b0838dd803d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 362,
                "y": 100
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1b619419-d066-431d-8c6e-1aa2b36951d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d6caefb2-aebf-48d0-a5f5-2046573c2638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 139,
                "y": 51
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1294eddd-9414-4189-81c6-33795ae439a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e4a140cc-59d4-4e76-92a7-385fd00cb822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 120,
                "y": 51
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "34108b28-71bb-4f39-91ed-10d3a8f53296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "61fe57cd-7161-4654-9b40-d414603391b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 47,
                "offset": 2,
                "shift": 19,
                "w": 18,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cc9704d6-a7ad-44f6-b8f5-cd193a9d8ae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 47,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 274,
                "y": 100
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a322c0cc-866d-4e94-9718-d7e082c57893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 82,
                "y": 51
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "abe1cc85-20da-4e04-be74-48490d7ba2bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 215,
                "y": 51
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ed7026fc-e7a6-4737-88ae-1ae285cb0437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 47,
                "offset": -2,
                "shift": 17,
                "w": 20,
                "x": 257,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "78ca8548-2926-4e52-8f21-1ffdad5938f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "16cc86c2-484b-4a47-8cbc-221497b225f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 301,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8a70f2a3-0582-43b0-9a81-d2d7696c353f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8b3033d1-14a3-4c47-ae5f-2f0eebec1a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 47,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 177,
                "y": 51
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ce839c8f-d486-435f-a5ed-72588f96b696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 338,
                "y": 100
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bbbcb664-047c-472b-b191-04e6e7be9401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 398,
                "y": 100
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4b8957f3-0922-4dda-841d-607d86327a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 386,
                "y": 100
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "bb8a7d1d-b0c8-47ad-b227-0607e0a7e674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 383,
                "y": 51
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4bed7b2a-bae3-478e-9a17-c2c8f0ad3edf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 365,
                "y": 51
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ef94b47c-ecda-4747-bbaf-06c9fcee4b3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 47,
                "offset": 3,
                "shift": 16,
                "w": 8,
                "x": 495,
                "y": 100
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b26e83b6-8ae0-4f22-848a-64dcf8822050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 62,
                "y": 51
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "acb2749c-bc2d-4232-8fff-a8ca9c409171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 234,
                "y": 51
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0b1f86d5-24c3-4f59-859d-173c7cf763a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 467,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e27b5efa-709d-471b-823b-28942a26d5a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 42,
                "y": 51
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7ae09972-bc31-4295-b233-ef66813bfa67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 22,
                "y": 51
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7bd06f6b-8126-44e3-8e52-667f788222f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 410,
                "y": 100
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "de0476e5-d4ec-48b1-b688-6a7d5bea5eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 158,
                "y": 51
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4d7b5397-1af2-4e63-9dcc-dad95e340151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 55,
                "y": 100
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b88e45d6-d777-416a-a7f4-cb7576ca23a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 41,
                "y": 149
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9b513cc3-b199-4c40-ae4b-93c381e44259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 47,
                "offset": -3,
                "shift": 11,
                "w": 13,
                "x": 203,
                "y": 100
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8b5a33e6-8a1b-4280-b062-f0876ac90b65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 196,
                "y": 51
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "de51af21-5e28-4234-9a59-28b937e8e95d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 149
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4018426d-3175-4f8d-9f25-0d7e3cab07cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7ef34aa5-e411-41ea-a259-52473269ce49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 72,
                "y": 100
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "12cfbd75-e1d2-420e-a9b5-bbb52ee7e7bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 407,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c8c9de8b-f0f3-4bc3-8c99-2c7029413647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 101,
                "y": 51
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "bbb1afd2-6ea9-4d44-8351-773935e65ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 427,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "37f8832a-c69d-4b65-b690-71c762d022ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 47,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 314,
                "y": 100
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d4e04f9e-452a-43cf-ba02-a627ff0ca351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 47,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 218,
                "y": 100
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4cf33623-3589-4dae-a778-ac624fe4bbd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 444,
                "y": 100
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7d937518-8aaf-4f43-9a18-e815aa8f69dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 38,
                "y": 100
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3b336a25-c89a-47e7-90ad-98fe038f9a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 47,
                "offset": -2,
                "shift": 12,
                "w": 16,
                "x": 473,
                "y": 51
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "06741a16-366a-45e5-ad9a-59115beb418c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "04bf2ccd-c104-4e39-a762-88b8d42cb709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 89,
                "y": 100
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f212ec20-df2c-4600-9dee-68fc60d1ae6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 106,
                "y": 100
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "594e1177-f73a-4786-8fd1-9eb348bc2d89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 20,
                "y": 100
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5d0d492f-d6a2-4956-bbdc-f2b3ff96eb40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 302,
                "y": 100
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e1bea682-ff8e-4d48-93f9-2e5dc97d24da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 47,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 76,
                "y": 149
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "057b658b-037e-4a1d-abbc-5feb767df3fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 374,
                "y": 100
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e899a55b-d5db-4e1e-ae5a-084cff0b1dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 253,
                "y": 51
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}