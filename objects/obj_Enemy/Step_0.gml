/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 37578713
/// @DnDArgument : "code" "vsp = vsp + grv;$(13_10)$(13_10)//Dont Walk Off edges$(13_10)if (grounded) && (afraidofheights) && (!place_meeting(x+hsp,y+1,obj_Wall))$(13_10){$(13_10)	hsp = -hsp;	$(13_10)}$(13_10)$(13_10)//Horizontal Collision$(13_10)if (place_meeting(x+hsp,y,spr_wall))$(13_10){$(13_10)	while (!place_meeting(x+sign(hsp),y	,spr_wall))$(13_10)	{$(13_10)		x = x + sign(hsp);$(13_10)	}$(13_10)	hsp = -hsp;$(13_10)}$(13_10)x = x + hsp;$(13_10)$(13_10)//Vertical Collision$(13_10)if (place_meeting(x,y+vsp,spr_wall))$(13_10){$(13_10)	while (!place_meeting(x,y+sign(vsp),spr_wall))$(13_10)	{$(13_10)		y = y + sign(vsp);$(13_10)	}$(13_10)	vsp = 0;$(13_10)}$(13_10)y = y + vsp;$(13_10)//Animation$(13_10)if (!place_meeting(x,y+1,spr_wall))$(13_10){$(13_10)	grounded = false;$(13_10)	sprite_index = spr_enemyA	$(13_10)	image_speed = 0;$(13_10)	if (sign(vsp) >	0) image_index = 1; else image_index = 0;$(13_10)}$(13_10)else$(13_10){$(13_10)	grounded = true;$(13_10)	image_speed = 1;$(13_10)	if (hsp == 0)$(13_10)	{$(13_10)			sprite_index = spr_enemy;$(13_10)	}$(13_10)	else$(13_10)	{$(13_10)			sprite_index = spr_enemyR$(13_10)	}$(13_10)}$(13_10)$(13_10)if (hsp != 0) image_xscale = sign(hsp);"
vsp = vsp + grv;

//Dont Walk Off edges
if (grounded) && (afraidofheights) && (!place_meeting(x+hsp,y+1,obj_Wall))
{
	hsp = -hsp;	
}

//Horizontal Collision
if (place_meeting(x+hsp,y,spr_wall))
{
	while (!place_meeting(x+sign(hsp),y	,spr_wall))
	{
		x = x + sign(hsp);
	}
	hsp = -hsp;
}
x = x + hsp;

//Vertical Collision
if (place_meeting(x,y+vsp,spr_wall))
{
	while (!place_meeting(x,y+sign(vsp),spr_wall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}
y = y + vsp;
//Animation
if (!place_meeting(x,y+1,spr_wall))
{
	grounded = false;
	sprite_index = spr_enemyA	
	image_speed = 0;
	if (sign(vsp) >	0) image_index = 1; else image_index = 0;
}
else
{
	grounded = true;
	image_speed = 1;
	if (hsp == 0)
	{
			sprite_index = spr_enemy;
	}
	else
	{
			sprite_index = spr_enemyR
	}
}

if (hsp != 0) image_xscale = sign(hsp);