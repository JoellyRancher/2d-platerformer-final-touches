/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 0403F853
/// @DnDArgument : "code" "/// @desc Move to next room$(13_10)$(13_10)with(obj_Player)$(13_10){$(13_10)	if (hascontrol)$(13_10)	{$(13_10)		hascontrol = false;$(13_10)		SlideTransition(TRANS_MODE.GOTO,other.target);$(13_10)	}$(13_10)}$(13_10)"
/// @desc Move to next room

with(obj_Player)
{
	if (hascontrol)
	{
		hascontrol = false;
		SlideTransition(TRANS_MODE.GOTO,other.target);
	}
}