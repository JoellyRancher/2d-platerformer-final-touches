{
    "id": "8d0fafa1-201a-4fea-aabc-bc502d88e706",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLevelEnd",
    "eventList": [
        {
            "id": "86a03023-6199-4336-a639-30e534efcf05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "9bd90aad-eefd-499e-a5da-e1024f06535f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d0fafa1-201a-4fea-aabc-bc502d88e706"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ddf9fd2-730c-4a6c-b4f3-260abeed454a",
    "visible": false
}