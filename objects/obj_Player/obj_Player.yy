{
    "id": "9bd90aad-eefd-499e-a5da-e1024f06535f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player",
    "eventList": [
        {
            "id": "221f2899-49ee-494f-8c83-e6bceca18738",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9bd90aad-eefd-499e-a5da-e1024f06535f"
        },
        {
            "id": "99476c1d-4064-4eaa-b61a-d5eb6944debc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9bd90aad-eefd-499e-a5da-e1024f06535f"
        },
        {
            "id": "de2afcc8-c148-4dfa-9ec9-e93eaca27b62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e1c0efcd-6d0b-4749-973b-869aa917ea4f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9bd90aad-eefd-499e-a5da-e1024f06535f"
        },
        {
            "id": "28b00f9e-a499-4ccf-8bfe-3d6958ee0c38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9bd90aad-eefd-499e-a5da-e1024f06535f"
        }
    ],
    "maskSpriteId": "8c8b57d6-31af-47fe-aae0-94a55030243d",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8c8b57d6-31af-47fe-aae0-94a55030243d",
    "visible": true
}