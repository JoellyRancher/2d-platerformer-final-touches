/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6D22FAC8
/// @DnDArgument : "code" "//Get  Player Input$(13_10)$(13_10)if (hascontrol)$(13_10){$(13_10)	key_left = keyboard_check(vk_left) || keyboard_check(ord("A"));$(13_10)	key_right = keyboard_check(vk_right) || keyboard_check(ord("D"));$(13_10)	key_jump = keyboard_check(vk_space);$(13_10)$(13_10)	//Calculate Movment$(13_10)	var move = key_right - key_left;$(13_10)$(13_10)	hsp = (move * walksp) + gunkickx;$(13_10)	gunkickx = 0;$(13_10)$(13_10)	vsp = (vsp + grv) + gunkicky;$(13_10)	gunkicky = -0.1;$(13_10)$(13_10)	if (place_meeting(x,y+1,spr_wall)) && (key_jump)$(13_10)	{$(13_10)		vsp = -7;$(13_10)	}$(13_10)}$(13_10)else$(13_10){$(13_10)	key_right = 0;$(13_10)	key_left = 0;$(13_10)	key_jump = 0;$(13_10)}$(13_10)//Horizontal Collision$(13_10)if (place_meeting(x+hsp,y,spr_wall))$(13_10){$(13_10)	while (!place_meeting(x+sign(hsp),y	,spr_wall))$(13_10)	{$(13_10)		x = x + sign(hsp);$(13_10)	}$(13_10)	hsp = 0;$(13_10)}$(13_10)x = x + hsp;$(13_10)$(13_10)//Vertical Collision$(13_10)if (place_meeting(x,y+vsp,spr_wall))$(13_10){$(13_10)	while (!place_meeting(x,y+sign(vsp),spr_wall))$(13_10)	{$(13_10)		y = y + sign(vsp);$(13_10)	}$(13_10)	vsp = 0;$(13_10)}$(13_10)y = y + vsp;$(13_10)//Animation$(13_10)if (!place_meeting(x,y+1,spr_wall))$(13_10){$(13_10)	sprite_index = spr_PlayerA;	$(13_10)	image_speed = 0;$(13_10)	if (sign(vsp) >	0) image_index = 1; else image_index = 0;$(13_10)}$(13_10)else$(13_10){$(13_10)	if (sprite_index == spr_PlayerA) audio_play_sound(snLanding,4,false);$(13_10)	image_speed = 1;$(13_10)	if (hsp == 0)$(13_10)	{$(13_10)			sprite_index = spr_Player;$(13_10)	}$(13_10)	else$(13_10)	{$(13_10)			sprite_index = spr_PlayerR$(13_10)	}$(13_10)}$(13_10)$(13_10)if (hsp != 0) image_xscale = sign(hsp);"
//Get  Player Input

if (hascontrol)
{
	key_left = keyboard_check(vk_left) || keyboard_check(ord("A"));
	key_right = keyboard_check(vk_right) || keyboard_check(ord("D"));
	key_jump = keyboard_check(vk_space);

	//Calculate Movment
	var move = key_right - key_left;

	hsp = (move * walksp) + gunkickx;
	gunkickx = 0;

	vsp = (vsp + grv) + gunkicky;
	gunkicky = -0.1;

	if (place_meeting(x,y+1,spr_wall)) && (key_jump)
	{
		vsp = -7;
	}
}
else
{
	key_right = 0;
	key_left = 0;
	key_jump = 0;
}
//Horizontal Collision
if (place_meeting(x+hsp,y,spr_wall))
{
	while (!place_meeting(x+sign(hsp),y	,spr_wall))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}
x = x + hsp;

//Vertical Collision
if (place_meeting(x,y+vsp,spr_wall))
{
	while (!place_meeting(x,y+sign(vsp),spr_wall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}
y = y + vsp;
//Animation
if (!place_meeting(x,y+1,spr_wall))
{
	sprite_index = spr_PlayerA;	
	image_speed = 0;
	if (sign(vsp) >	0) image_index = 1; else image_index = 0;
}
else
{
	if (sprite_index == spr_PlayerA) audio_play_sound(snLanding,4,false);
	image_speed = 1;
	if (hsp == 0)
	{
			sprite_index = spr_Player;
	}
	else
	{
			sprite_index = spr_PlayerR
	}
}

if (hsp != 0) image_xscale = sign(hsp);