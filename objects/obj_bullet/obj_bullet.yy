{
    "id": "ff5e2a2b-3cb7-4f8b-8934-d66a19089cae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "9936b0eb-f6cc-49f1-8ae5-f30f0ee4a578",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ff5e2a2b-3cb7-4f8b-8934-d66a19089cae"
        },
        {
            "id": "a9a4e78d-f0e9-405a-9251-e28aab9c1f83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "ff5e2a2b-3cb7-4f8b-8934-d66a19089cae"
        },
        {
            "id": "00a512a7-aeda-4a53-844f-7802e67c5676",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e1c0efcd-6d0b-4749-973b-869aa917ea4f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ff5e2a2b-3cb7-4f8b-8934-d66a19089cae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "79c18cc0-6531-41ad-880e-78ebb2454c7f",
    "visible": true
}