/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 585BB39B
/// @DnDArgument : "code" "/// @desc Control Menu$(13_10)$(13_10)///Item ease in$(13_10)menu_x += (menu_x_target - menu_x) / menu_speed;$(13_10)$(13_10)//keyboard controls$(13_10)if (menu_control)$(13_10){$(13_10)	if (keyboard_check_pressed(vk_up))$(13_10)	{$(13_10)		menu_cursor++;$(13_10)		if (menu_cursor >= menu_items) menu_cursor = 0;$(13_10)	}$(13_10)	$(13_10)	if (keyboard_check_pressed(vk_down))$(13_10)	{$(13_10)		menu_cursor--;$(13_10)		if (menu_cursor < 0) menu_cursor = menu_items-1;$(13_10)	}$(13_10)	$(13_10)	if (keyboard_check_pressed(vk_enter))$(13_10)	{$(13_10)		menu_x_target = gui_width+200;$(13_10)		menu_commited = menu_cursor;$(13_10)		ScreenShake(4,30);$(13_10)		menu_control = false$(13_10)		audio_play_sound(snDeath,10,false)$(13_10)	}$(13_10)}$(13_10)$(13_10)if (menu_x > gui_width+150) && (menu_commited != -1)$(13_10){$(13_10)	switch (menu_commited)$(13_10)	{$(13_10)		case 2: SlideTransition(TRANS_MODE.NEXT); break;$(13_10)		case 0: game_end(); break;$(13_10)	}$(13_10)}"
/// @desc Control Menu

///Item ease in
menu_x += (menu_x_target - menu_x) / menu_speed;

//keyboard controls
if (menu_control)
{
	if (keyboard_check_pressed(vk_up))
	{
		menu_cursor++;
		if (menu_cursor >= menu_items) menu_cursor = 0;
	}
	
	if (keyboard_check_pressed(vk_down))
	{
		menu_cursor--;
		if (menu_cursor < 0) menu_cursor = menu_items-1;
	}
	
	if (keyboard_check_pressed(vk_enter))
	{
		menu_x_target = gui_width+200;
		menu_commited = menu_cursor;
		ScreenShake(4,30);
		menu_control = false
		audio_play_sound(snDeath,10,false)
	}
}

if (menu_x > gui_width+150) && (menu_commited != -1)
{
	switch (menu_commited)
	{
		case 2: SlideTransition(TRANS_MODE.NEXT); break;
		case 0: game_end(); break;
	}
}