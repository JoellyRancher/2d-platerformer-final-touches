{
    "id": "b98802c5-1490-4e37-9223-32a2c79f06a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PDead",
    "eventList": [
        {
            "id": "4d38125e-e508-452c-88d4-eec29e0690a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b98802c5-1490-4e37-9223-32a2c79f06a7"
        },
        {
            "id": "25dd3fe6-a3e9-410d-b332-f04f49364352",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b98802c5-1490-4e37-9223-32a2c79f06a7"
        },
        {
            "id": "d3fb42b5-42f6-4442-8ad8-32a4cf687f5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b98802c5-1490-4e37-9223-32a2c79f06a7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "10991b3b-015d-4bce-8eee-e1d043ac1b88",
    "visible": true
}