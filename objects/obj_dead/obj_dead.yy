{
    "id": "529d6299-41f0-47be-ad6f-ea42904ca0f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dead",
    "eventList": [
        {
            "id": "6ec2baf1-4d06-42bd-a748-b9b36e6b6322",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "529d6299-41f0-47be-ad6f-ea42904ca0f3"
        },
        {
            "id": "5f0f208b-95ca-4cfe-a9de-e58942f952ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "529d6299-41f0-47be-ad6f-ea42904ca0f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a932325c-afab-4e5c-8506-afedc0048711",
    "visible": true
}