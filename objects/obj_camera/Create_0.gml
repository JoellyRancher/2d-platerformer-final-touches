/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 64BC1AA1
/// @DnDArgument : "code" "///@description set up camera$(13_10)cam = view_camera[0];$(13_10)follow = obj_Player;$(13_10)view_w_half = camera_get_view_width(cam) * 0.2;$(13_10)view_h_half = camera_get_view_height(cam) * 0.6;$(13_10)xTo = xstart;$(13_10)yTo = ystart;$(13_10)$(13_10)shake_length = 0;$(13_10)shake_magnitude = 0;$(13_10)shake_remain = 0;$(13_10)buff = 32;"
///@description set up camera
cam = view_camera[0];
follow = obj_Player;
view_w_half = camera_get_view_width(cam) * 0.2;
view_h_half = camera_get_view_height(cam) * 0.6;
xTo = xstart;
yTo = ystart;

shake_length = 0;
shake_magnitude = 0;
shake_remain = 0;
buff = 32;