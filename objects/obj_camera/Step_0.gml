/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 21C5CE19
/// @DnDArgument : "code" "///@description update camera$(13_10)$(13_10)//update destination$(13_10)if (instance_exists(follow))$(13_10){$(13_10)	xTo = follow.x;$(13_10)	yTo = follow.y;$(13_10)	$(13_10)	if ((follow).object_index == obj_PDead)$(13_10)	{$(13_10)		x = xTo;$(13_10)		y = yTo;$(13_10)	}$(13_10)}$(13_10)$(13_10)//update object position$(13_10)x += (xTo - x) / 25;$(13_10)y += (yTo - y) / 25;$(13_10)$(13_10)//keep camera center inside room$(13_10)x = clamp(x,view_w_half,room_width-view_w_half);$(13_10)y = clamp(y,view_h_half,room_height-view_h_half);$(13_10)$(13_10)//ScreenShake$(13_10)x += random_range(-shake_remain,shake_remain);$(13_10)y += random_range(-shake_remain,shake_remain);$(13_10)shake_remain = max(0,shake_remain-((1/shake_length)*shake_magnitude));$(13_10)$(13_10)//update Camera view$(13_10)camera_set_view_pos(cam,x-view_w_half,y-view_h_half);$(13_10)$(13_10)if (layer_exists("mountains"))$(13_10){$(13_10)	layer_x("mountains",x/2)$(13_10)}$(13_10)$(13_10)if (layer_exists("trees"))$(13_10){$(13_10)	layer_x("trees",x/4)$(13_10)}"
///@description update camera

//update destination
if (instance_exists(follow))
{
	xTo = follow.x;
	yTo = follow.y;
	
	if ((follow).object_index == obj_PDead)
	{
		x = xTo;
		y = yTo;
	}
}

//update object position
x += (xTo - x) / 25;
y += (yTo - y) / 25;

//keep camera center inside room
x = clamp(x,view_w_half,room_width-view_w_half);
y = clamp(y,view_h_half,room_height-view_h_half);

//ScreenShake
x += random_range(-shake_remain,shake_remain);
y += random_range(-shake_remain,shake_remain);
shake_remain = max(0,shake_remain-((1/shake_length)*shake_magnitude));

//update Camera view
camera_set_view_pos(cam,x-view_w_half,y-view_h_half);

if (layer_exists("mountains"))
{
	layer_x("mountains",x/2)
}

if (layer_exists("trees"))
{
	layer_x("trees",x/4)
}