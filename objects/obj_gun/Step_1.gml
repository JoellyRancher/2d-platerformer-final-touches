/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 080A927D
/// @DnDArgument : "code" "x = obj_Player.x;$(13_10)y = obj_Player.y+10;$(13_10)$(13_10)image_angle = point_direction(x,y,mouse_x,mouse_y);$(13_10)$(13_10)firingdelay = firingdelay - 1;$(13_10)recoil = max(0, recoil -1);$(13_10)$(13_10)if (mouse_check_button(mb_left)) && (firingdelay < 0)$(13_10){$(13_10)	recoil = 4;$(13_10)	firingdelay = 5;$(13_10)	ScreenShake(3,16);$(13_10)	audio_play_sound(Shoot,1000,false)$(13_10)	with (instance_create_layer(x,y,"bullets",obj_bullet))$(13_10)	{$(13_10)		speed = 25;$(13_10)		direction = other.image_angle;$(13_10)		image_angle = direction;$(13_10)	}$(13_10)	$(13_10)	with(obj_Player)$(13_10)	{$(13_10)		gunkickx = lengthdir_x(1.5, other.image_angle-180);$(13_10)		gunkicky = lengthdir_y(1, other.image_angle-180);$(13_10)	}$(13_10)}$(13_10)$(13_10)x = x - lengthdir_x(recoil, image_angle);$(13_10)y = y - lengthdir_y(recoil, image_angle);$(13_10)$(13_10)if (image_angle > 90) && (image_angle < 270)$(13_10){$(13_10)	image_yscale = -1;$(13_10)}$(13_10)else$(13_10){$(13_10)	image_yscale = 1;$(13_10)}"
x = obj_Player.x;
y = obj_Player.y+10;

image_angle = point_direction(x,y,mouse_x,mouse_y);

firingdelay = firingdelay - 1;
recoil = max(0, recoil -1);

if (mouse_check_button(mb_left)) && (firingdelay < 0)
{
	recoil = 4;
	firingdelay = 5;
	ScreenShake(3,16);
	audio_play_sound(Shoot,1000,false)
	with (instance_create_layer(x,y,"bullets",obj_bullet))
	{
		speed = 25;
		direction = other.image_angle;
		image_angle = direction;
	}
	
	with(obj_Player)
	{
		gunkickx = lengthdir_x(1.5, other.image_angle-180);
		gunkicky = lengthdir_y(1, other.image_angle-180);
	}
}

x = x - lengthdir_x(recoil, image_angle);
y = y - lengthdir_y(recoil, image_angle);

if (image_angle > 90) && (image_angle < 270)
{
	image_yscale = -1;
}
else
{
	image_yscale = 1;
}