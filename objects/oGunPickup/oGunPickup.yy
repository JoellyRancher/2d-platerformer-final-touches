{
    "id": "822c421b-25a8-4d2a-80c0-44d469f999ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGunPickup",
    "eventList": [
        {
            "id": "c44bb660-0ac1-4725-ab4c-74dd70b0640d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "822c421b-25a8-4d2a-80c0-44d469f999ab"
        },
        {
            "id": "251238d0-dacd-4531-971a-86811125fb3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "9bd90aad-eefd-499e-a5da-e1024f06535f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "822c421b-25a8-4d2a-80c0-44d469f999ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e4c4cc0d-8e4f-4d5c-8d4f-eba23c35b8f1",
    "visible": true
}