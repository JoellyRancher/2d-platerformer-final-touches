{
    "id": "0877d8f0-f6a8-40f1-a501-dad5b1812cfa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTransition",
    "eventList": [
        {
            "id": "8ee515c2-c331-48d9-b74c-c98fcd826ade",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0877d8f0-f6a8-40f1-a501-dad5b1812cfa"
        },
        {
            "id": "e16d0a93-156e-44fa-be32-b279727ed5ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0877d8f0-f6a8-40f1-a501-dad5b1812cfa"
        },
        {
            "id": "64e82891-6cb2-41ba-859f-61529617a488",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0877d8f0-f6a8-40f1-a501-dad5b1812cfa"
        },
        {
            "id": "c700f3d9-cf45-43f2-a21f-b35480717fa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "0877d8f0-f6a8-40f1-a501-dad5b1812cfa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}